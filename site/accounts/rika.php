<?php if(!defined('KIRBY')) exit ?>

username: rika
firstname: rikha
lastname: khusnia
email: RikaLuvIslam@gmail.com
password: >
  $2a$10$3PWMXXX19C5QcVUd2OVzMO.UODNppH8CWwA8bWOrmG/2Qw8lKn0ea
language: en
role: admin
history:
  - >
    informasi/berita/pasar-oro-oro-dowo-pasar-percontohan-nasional
  - informasi/berita/pelatihan-pembuatan-produk-gypsum-di-kota-malang
  - informasi/berita/berita-baru
  - informasi/berita/bordir
  - informasi/berita/pelepah-pisang
