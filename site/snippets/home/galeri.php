<!-- TESTIMONIALS 4 -->
<div class="page-section p-130-cont bg-gray">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <div class="mb-70">
          <h2 class="section-title2 font-light font-signpainter text-center p-0">Pengaduan Online</h2>
        </div>
      </div>

    </div>
    <div class="row">
<div class="col-md-6">
  <div class="col-md-12">
          <h4 class="pb-20">Berita Terbaru</h4>
      </div>
<?php foreach(page('informasi')->children()->find('berita')->children()->limit(2) as $berita): ?>

            <!-- Post Item 1 -->
            <div class="col-sm-6 col-md-6 col-lg-6 wow fadeIn pb-70" >
                <div class="post-prev-img">
                    <a href="<?php echo $site->url() ?>/informasi/berita">
                        <?php if($image = $berita->images()->sortBy('sort', 'asc')->first()): ?>
                            <img src="<?php echo $image->url() ?>" alt="">
                        <?php endif; ?>
                        <!-- <img src="images/blog/blog-sect3-post-anim.gif" alt="img"> -->
                    </a>
                </div>
                <div class="post-prev-title">
                    <h3>
                        <a href="<?php echo $berita->url() ?>"><?php echo $berita->title()->html() ?></a>
                    </h3>
                </div>
                <div class="post-prev-info">
                    <?php echo date('d M Y', $berita->date()) ?>
                    <span class="slash-divider">/</span>
                    <a href="<?php echo $berita->parent()->url() ?>">Berita</a>
                </div>
                <!-- <div> -->
                    <?php //echo $berita->text()->excerpt(100) ?>
                <!-- </div> -->
            </div>
            
            <?php endforeach; ?>
      </div>
      <div class="col-md-6">
        <div class="col-md-12">
          <h4 class="pb-20">News in Picture</h4>
      </div>
      <div class="col-md-12">
        <ul class="port-grid masonry clearfix" id="items-grid">
                
                <!-- Item big -->
                <li class="port-item mix design lightbox-item">
                  <a href="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-23-big-box.jpg" class="lightbox">
                    <div class="port-img-overlay">
                      <img class="port-main-img" src="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-23-big-box.jpg" alt="img" >
                    </div>
                    <div class="port-overlay-cont">
                      <div class="port-btn-cont">
                        <div aria-hidden="true" class="icon_search"></div>
                      </div>
                    </div>
                  </a>
                </li>
                                             
                <!-- Item -->
                <li class="port-item mix design lightbox-item">
                  <a href="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-20-box.jpg" class="lightbox">
                    <div class="port-img-overlay">
                      <img class="port-main-img" src="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-20-box.jpg" alt="img" >
                    </div>
                    <div class="port-overlay-cont">
                      <div class="port-btn-cont">
                        <div aria-hidden="true" class="icon_search"></div>
                      </div>
                    </div>
                  </a>
                </li>
                         
                <!-- Item big -->
                <li class="port-item mix design lightbox-item">
                  <a href="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-24-big-box.jpg" class="lightbox">
                    <div class="port-img-overlay">
                      <img class="port-main-img" src="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-24-big-box.jpg" alt="img" >
                    </div>
                    <div class="port-overlay-cont">
                      <div class="port-btn-cont">
                        <div aria-hidden="true" class="icon_search"></div>
                      </div>
                    </div>
                  </a>
                </li>

                <!-- Item -->
                <li class="port-item mix design lightbox-item">
                  <a href="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-22-box.jpg" class="lightbox">
                    <div class="port-img-overlay">
                      <img class="port-main-img" src="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-22-box.jpg" alt="img" >
                    </div>
                    <div class="port-overlay-cont">
                      <div class="port-btn-cont">
                        <div aria-hidden="true" class="icon_search"></div>
                      </div>
                    </div>
                  </a>
                </li>
                               
                <!-- Item -->
                <li class="port-item mix design lightbox-item">
                  <a href="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-21-box.jpg" class="lightbox">
                    <div class="port-img-overlay">
                      <img class="port-main-img" src="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-21-box.jpg" alt="img" >
                    </div>
                    <div class="port-overlay-cont">
                      <div class="port-btn-cont">
                        <div aria-hidden="true" class="icon_search"></div>
                      </div>
                    </div>
                  </a>
                </li>
                
                <!-- Item -->
                <li class="port-item mix design lightbox-item">
                  <a href="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-25-box.jpg" class="lightbox">
                    <div class="port-img-overlay">
                      <img class="port-main-img" src="<?php echo $site->url() ?>/assets/vendor/images/portfolio/projects-25-box.jpg" alt="img" >
                    </div>
                    <div class="port-overlay-cont">
                      <div class="port-btn-cont">
                        <div aria-hidden="true" class="icon_search"></div>
                      </div>
                    </div>
                  </a>
                </li>

              </ul>
              </div>
      </div>
      

      
  </div>
  </div>
</div>