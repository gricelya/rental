<h4 class="blog-page-title mt-50 mb-25">Related Posts</h4>
<div class="row related-posts">

    
    <!-- Post Item 1 -->
    <div class="col-sm-6 col-md-4 col-lg-4 wow fadeIn pb-50" >
        <div class="post-prev-img">
            <a href="blog-single-sidebar-right.html"><img src="images/blog/post-prev-1.jpg" alt="img"></a>
        </div>
        <div class="post-prev-title">
            <h3><a href="blog-single-sidebar-right.html">Time For Minimalism</a></h3>
        </div>
        <div class="post-prev-info">
            Jule 10<span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">John Doe</a>
        </div>
    </div>
</div>