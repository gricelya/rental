<div class="widget">
    <h5 class="widget-title2"><?php echo ucfirst($type) ?> Terbaru</h5>
    <div class="widget-body mt-25">
        <ul class="clearlist widget-posts">
            
            <?php foreach(page('informasi')->children()->find(strtolower($type))->children()->limit(4)->visible() as $post): ?>
                <li class="clearfix">
                    <?php if($type == 'kegiatan') : ?>
                    <?php if($image = $post->images()->sortBy('sort', 'asc')->first()): ?>
                        <a href="<?php echo $post->url() ?>">
                            <img src="<?php echo $image->url() ?>" alt="img">
                        </a>
                    <?php endif; ?>
                    <?php endif; ?>
                    
                    <div class="widget-posts-descr2">
                        <a href="<?php echo $post->url() ?>" class="font-poppins lh-18">
                            <?php echo $post->title() ?>
                        </a>
                        <div class="lh-18"><?php echo date('d M Y', $post->date()) ?></div> 
                    </div>
                </li>
            <?php endforeach; ?>

        </ul>
    </div>
</div>