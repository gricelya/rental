<div class="container p-140-cont">
    <div class="row">
    <?php $parent_title = strtolower($page->parent()->title()) ?>

    <?php if($parent_title == 'berita'): ?>
        <?php include_once('blog-detail-mainbar.php') ?>
    <?php else: ?>
        <?php include_once('blog-mainbar.php') ?>
    <?php endif; ?>
    
    <?php include_once('blog-sidebar.php') ?>

    </div>

    <?php if( strtolower($page->title()) == 'berita'): ?>
    <?php include_once('blog-pagination.php') ?>
    <?php endif; ?>

</div>