<div class="col-sm-8 blog-main-posts">
          
    <!-- Post Item -->
    <div class="wow fadeIn pb-90">
        
        <?php if($page->parent()->uid() != "pengumuman"): ?>
        <div class="post-prev-img">
            <?php if($image = $page->images()->sortBy('sort', 'asc')->first()): ?>
                <a href="<?php echo $page->url() ?>">
                    <img src="<?php echo $image->url() ?>" alt="img">
                </a>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <div class="post-prev-title">
            <h3 class="post-title-big">
                <a href="<?php echo $page->url() ?>">
                    <?php echo $page->title() ?>
                </a>
            </h3>
        </div>

        <div class="post-prev-info ">
            <?php echo date('d M Y', $page->date()) ?>
            <!-- <span class="slash-divider">/</span> -->
            <!-- <a href="">Administrator</a> -->
            <!-- <span class="slash-divider">/</span> -->
            <!-- <a href="#">Design</a>,  -->
            <!-- <a href="#">Trends</a> -->
        </div>

        <div class="mb-30">
            <?php echo $page->text()->kirbytext() ?>
        </div>
        
        <?php if($page->parent()->uid() == "pengumuman"): ?>

        <div class="files mb-30">
            <h4>Tautan berkas terlampir:</h4>

            <table class="table mb-40">
                <thead>
                <tr>
                  <th>Nama File</th>
                  <th>Jenis File</th>
                  <th>Ukuran File</th>
                  <th>Link Download</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($page->files() as $file): ?>
                <tr>
                  <td>
                    <strong>
                      <?php echo $file->filename() ?>
                    </strong>
                </td>
                  <td><?php echo $file->extension() ?> (<?php echo $file->type() ?>)</td>
                  <td><?php echo $file->niceSize() ?></td>
                  <td><a href="<?php echo $file->url() ?>" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Unduh File</a></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
        </div>

        <?php endif; ?>
        
        <!-- Comment count, love, and share widget should goes here -->
        <?php //echo snippet('widget/mainbar/blog/share') ?>   

        <!-- Post author should goes here -->
        
        <?php include_once('blog-detail-pagination.php') ?>   
        <br>    
        <?php //echo snippet('widget/mainbar/blog/related-post') ?>   
                
        <?php echo snippet('widget/mainbar/blog/disqus-comment') ?>   
                
                
              
    </div>
</div>