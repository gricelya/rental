<div class="col-sm-8 blog-main-posts">

    <?php foreach($page->children()->limit(5) as $blogs): ?>
    
    <div class="wow fadeIn pb-90">
        
        <?php if($page->uid() != "pengumuman"): ?>
        <div class="post-prev-img">
            <?php if($image = $blogs->images()->sortBy('sort', 'asc')->first()): ?>
                <a href="<?php echo $blogs->url() ?>">
                    <img src="<?php echo $image->url() ?>" alt="img">
                </a>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <div class="post-prev-title">
            <h3 class="post-title-big">
                <a href="<?php echo $blogs->url() ?>">
                    <?php echo $blogs->title() ?>
                </a>
            </h3>
        </div>

        <div class="post-prev-info ">
            <?php echo date('d M Y', $blogs->date()) ?>
            <!-- <span class="slash-divider">/</span>
            <a href="">Administrator</a>
            <span class="slash-divider">/</span>
            <a href="#">Design</a>, 
            <a href="#">Trends</a> -->
        </div>

        <div class="mb-30">
            <?php echo $blogs->text()->excerpt(200) ?>
        </div>

        <div class="post-prev-more-cont clearfix">
            <div class="post-prev-more left">
                <a href="<?php echo $blogs->url() ?>" class="font-poppins button rounded medium gray">Read More</a>
            </div>
            <!-- <div class="right" >
                <a href="blog-single-sidebar-right.html#comments" class="post-prev-count">
                    <span aria-hidden="true" class="icon_comment_alt"></span>
                    <span class="icon-count">21</span>
                </a>
                <a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" class="post-prev-count">
                    <span aria-hidden="true" class="icon_heart_alt"></span>
                    <span class="icon-count">53</span>
                </a>
                <a href="#" class="post-prev-count dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
                    <span aria-hidden="true" class="social_share"></span>
                </a>
                <ul class="social-menu dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                        <a href="#">
                            <span aria-hidden="true" class="social_facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span aria-hidden="true" class="social_twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span aria-hidden="true" class="social_dribbble"></span>
                        </a>
                    </li>
                </ul>
            </div> -->
        </div>
    </div>

    <?php endforeach; ?>
</div>