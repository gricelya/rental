<?php $featured_image = $page->contentURL() . "/" . $page->featured_image(); ?>

<div class="page-title-cont page-title-big page-title-img bg-gray-dark" style="background-image: url(<?php echo $featured_image ?>)">
  <div class="dark-mask">&nbsp;</div>
          <div class="relative container align-left">
            <div class="row">
              
              <div class="col-md-8">
                <h1 class="page-title"><?php echo $page->title() ?></h1>
                <div class="page-sub-title font-poppins">
                  <i class="fa fa-map-marker"></i> <?php echo $page->location() ?>
              <span class="slash-divider">/</span>
              <i class="fa fa-calendar"></i> <?php echo date('d M Y', $page->date()) ?>
                </div>
              </div>
              
              <!-- <div class="col-md-4">
                <div class="breadcrumbs font-poppins">
                  <a class="a-inv" href="index-2.html">home</a><span class="slash-divider">/</span><span class="bread-current">page title big</span>
                </div>
              </div> -->
              
            </div>
          </div>

        </div>


<!-- CONTENT -->
<div class="page-section p-140-cont pb-50">
  <div class="container">

    <!-- PROJECT DETAILS & TEXT -->
    <div class="row">

      <div class="col-md-3 mb-50">
                <div class="lightbox-item">
                <a href="<?php echo $featured_image ?>" class="lightbox">
                  <div class="port-img-overlay">
                    <img class="port-main-img" src="<?php echo $featured_image ?>" alt="img">
                  </div>
                  <div class="port-overlay-cont">
                    <div class="port-btn-cont">
                      <div aria-hidden="true" class="icon_search"></div>
                    </div>
                  </div>
                </a>  

              </div>

              <!-- <hr class="mt-0 mb-0"> -->
              <br>
                <!-- PROJECT DETAILS -->
                <div class="port-detail-cont mb-30">
                  
                  <div class="port-detail font-poppins mb-20">
                    <p>
                      <strong class="font-black">Tanggal:</strong>
                        <?php echo date('d M Y', $page->date()) ?>
                    </p>
                    <p>
                      <strong class="font-black">Jam:</strong>
                        <?php echo $page->time() ?>
                    </p>
                    <p>
                      <strong class="font-black">Tempat:</strong>
                        <?php echo $page->location() ?>
                    </p>
                    <p>
                      <strong class="font-black">HTM:</strong>
                        <?php echo $page->price() ?>
                    </p>
                    <p>&nbsp;</p>
                    <?php if($page->is_open() == 1): ?>
                    <p>
                      <?php
                      if($page->is_open() == "1") :
                        if($page->registration_url() != "") {
                          $registration_url = $page->registration_url();
                        } else {
                          $id = base64_encode($page->title() . "|" . $page->uid() . "|" . date('d M Y', $page->date()) . "|" . $page->location());
                          $registration_url = $site->url() . "/registrasi/event?ref=" . $id;
                        }
                      ?>
                      <a class="button thin full-rounded large gray btn-block btn" href="<?php echo $registration_url ?>">Daftar Event</a>
                      <?php endif; ?>
                    </p>
                  <?php endif; ?>
                  </div>
                  
                 
                  
                </div>
              
              </div>

      <!-- TEXT -->
      <div class="col-md-9 mb-50">
        <p class="text-highlight mb-30">
          <?php echo $page->lead() ?>
        </p>
        <p>
          <?php echo $page->text() ?>
        </p>
        <!-- <h4 class="font-open-sans"><strong>Maecenas volutpat</strong></h4> -->
        <!-- <p>Cras tellus enim, sagittis aer varius faucibus, molestie in dolor. Mauris molliadipisg elit, in vulputate est volutpat vitae. Pellentesque convallis nisl sit amet lacus luctus vel consequat ligula suscipit. Aliquam et metus sed tortor eleifend pretium non id urna. Fusce in augue leo, sed cursus nisl. Nullam vel tellus massa. Vivamus porttitor rutrum libero ac mattis. Aliquam congue malesuada mauris vitae dignissim.</p> -->
      </div>

    </div>



  </div><!-- container end -->

 

  <!-- TEXT & INFO -->
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-md-offset-3 mt-30">


      </div>
    </div>

    <!-- DIVIDER -->
    <hr class="mt-0 mb-0">

    <!-- WORK NAVIGATION -->
    <div class="row">
      <div class="col-md-12">
        <!-- WORK NAVIGATION -->
    <div class="work-navigation clearfix">
        
        <?php if($page->hasPrev()): ?>
        <a href="<?php echo $page->prev()->url() ?>" class="work-prev">
        <?php else: ?>
        <a class="disable work-prev">
        <?php endif; ?>
            <span>
                <span class="icon icon-arrows-left"></span>
                &nbsp;Event Sebelumnya
            </span>
        </a>

        <a href="<?php echo $page->parent()->url() ?>" class="work-all"><span>Lihat Seluruh Event</span></a>
        
        <?php if($page->hasNext()): ?>
        <a href="<?php echo $page->next()->url() ?>" class="work-next">
        <?php else: ?>
        <a class="disable work-next">
        <?php endif; ?>
            <span>
                Event Berikutnya&nbsp;
                <span class="icon icon-arrows-right"></span>
            </span>
        </a>

    </div>
      </div>
    </div>

    <!-- DIVIDER -->
    <hr class="mt-0 mb-0">

  </div><!-- container end -->

</div><!-- page section end