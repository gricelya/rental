<div class="page-section">
    <div class="container-fluid">
        <div class="row">
            
            <?php
            $datas = base64_decode($_GET['ref']); 
            $data = explode("|", $datas);
            $title = $data[0];
            $uid = $data[1];
            $date = $data[2];
            $location = $data[3];
            ?>
            
            <div class="col-md-5">
                <div class="contact-form-cont">
                    <div class="mb-40">
                        <h3>Detail Event</h3>
                    </div>
                    <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-home"></div>
                  </div>
                  <div class="cis-text">
                    <p class="fes14-tab-text">Nama Kegiatan</p>
                    <h3><?php echo $title ?></h3>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-geolocalize-05"></div>
                  </div>
                  <div class="cis-text">
                    <p class="fes14-tab-text">Waktu</p>
                    <h3><?php echo $date ?></h3>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-6">
                <div class="cis-cont">
                  <div class="cis-icon">
                    <div class="icon icon-basic-mail"></div>
                  </div>
                  <div class="cis-text">
                    <p><a href="">Tempat</a></p>
                    <h3><?php echo $location ?></h3>
                  </div>
                </div>
              </div>
  
                </div>
            </div>


            <div class="col-md-7">
                <div class="contact-form-cont">
                    <!-- TITLE -->
                    <div class="mb-40">
                        <h3 >Isikan Data Diri Anda</h3>
                    </div>

                    <!-- CONTACT FORM -->
                    <div >
                        <form id="contact-form" action="<?php echo $site->url() ?>/registrasi/event/process" method="POST">
                            
                            <label>(*) Wajib Diisi</label>
                            <br>
                            <br>
                            <input type="hidden" name="id_registration" value="<?php echo md5($uid . date('YMdhms') . $location); ?>">
                            <input type="hidden" name="event_id" value="<?php echo $uid; ?>-<?php echo md5($uid) ?>">
                            <input type="hidden" name="event_title" value="<?php echo $title; ?>">

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Nama (*)</label>
                                    <input type="text" name="nama" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>No Identitas (KTP/SIM) (*)</label>
                                    <input type="text" name="no_identitas" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Email</label>
                                    <input type="email" name="email" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Telp (*)</label>
                                    <input type="text" name="telp" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-40">
                                    <label>Alamat (*)</label>
                                    <textarea name="alamat" maxlength="5000" rows="3" class="form-control" placeholder="" required></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 ">
                                    <input type="submit" value="DAFTAR SEKARANG" class="button medium rounded gray font-open-sans" data-loading-text="Loading...">
                                </div>
                            </div>

                        </form>   
                        <div class="alert alert-success hidden" id="contactSuccess">
                            <strong>Success!</strong> Your message has been sent to us.
                        </div>

                        <div class="alert alert-danger hidden" id="contactError">
                            <strong>Error!</strong> There was an error sending your message.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>