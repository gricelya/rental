<!-- COTENT CONTAINER -->
<div class="container p-140-cont pt-xxs-80">
  <div class="relative">
    <!-- PORTFOLIO FILTER -->                    
    <div>
      <?php 
      // $arr = array('a', 'b', 'c');
      // $json = json_encode($arr);
      // print_r($json);
       ?>
    <?php
    $tagStore = array();
    foreach($page->children() as $subpage) {
      $tags = explode(",", $subpage->tags());
      foreach($tags as $tag) {
        if(!in_array(strtolower($tag), $tagStore)) {
          array_push($tagStore, strtolower($tag));
        }
      }
    }
    // print_r($tagStore);
    ?>

      <ul class="port-filter font-poppins">
        <li>
          <a href="#" class="filter active" data-filter="*">Semua</a>
        </li>
        <?php foreach($tagStore as $tag) : ?>
        <li>
          <a href="#" class="filter" data-filter=".<?php echo $tag ?>"><?php echo ucfirst($tag); ?></a>
        </li>
        <?php endforeach; ?>
      </ul>

    </div>                   
    <!-- ITEMS GRID -->
    <ul class="port-grid port-grid-gut clearfix" id="items-grid">
      
      <?php foreach($page->children() as $album) : ?>
      <!-- Item 1 -->
      <?php
      $album_tags = str_replace(",", " ", $album->tags());
      ?>
      <li class="port-item mix <?php echo strtolower($album_tags) ?>">
        <a href="">
          <div class="port-img-overlay">
            <?php if($image = $album->images()->sortBy('sort', 'asc')->first()): ?>
              <img class="port-main-img" src="<?php echo $image->url() ?>" alt="img" >
            <?php endif; ?>
        </div>
        <div class="port-overlay-cont">
          <div class="port-title-cont2">
            <h3><?php echo $album->title() ?></h3>
            <span><?php echo date('d M Y', $album->date()) ?></span>
          </div>
        </div>
      </a>
    </li>
    <?php endforeach; ?>


  </ul>

</div>

</div>