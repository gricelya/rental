<div class="page-section">
    <div class="container-fluid">
        <div class="row">



            <div class="col-md-6">
                <div class="contact-form-cont">
                    <!-- TITLE -->
                    <div class="mb-40">
                        <h3 >Isikan Data IKM Anda</h3>
                    </div>

                    <!-- CONTACT FORM -->
                    <div >
                        <form id="contact-form" action="<?php echo $site->url() ?>/registrasi/ikm/process" method="POST">
                            
                            <label>(*) Wajib Diisi</label>
                            <br>
                            <br>
                            <input type="hidden" name="id_ikm" value="<?php echo md5(date('Ymdhms')); ?>">
                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Jenis Badan Usaha (*)</label>
                                    <input type="text" name="badan_usaha" placeholder="Isikan jenis badan usaha Anda (CV/PT/Lainnya)" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Nama Perusahaan (*)</label>
                                    <input type="text" name="nama_perusahaan" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Nama Pemilik (*)</label>
                                    <input type="text" name="nama_pemilik" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-40">
                                    <label>Alamat (*)</label>
                                    <textarea name="alamat" maxlength="5000" rows="3" class="form-control" placeholder="" required></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Desa/Kelurahan</label>
                                    <input type="text" name="desa_kelurahan" placeholder="" value="" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Kecamatan (*)</label>
                                    <input type="text" name="kecamatan" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Telepon (*)</label>
                                    <input type="text" name="telepon" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Fax</label>
                                    <input type="text" name="fax" placeholder="" value="" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Email</label>
                                    <input type="email" name="email" placeholder="" value="" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Alamat Situs / Website</label>
                                    <input type="text" name="website" placeholder="" value="" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Ijin UI (*)</label>
                                    <input type="text" name="ijin_ui" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Tahun dikeluarkan ijin (*)</label>
                                    <input type="text" name="tahun_ijin_ui" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>KBLI Kode (*)</label>
                                    <input type="text" name="kbli_kode" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Komoditi / Nama Produk (*)</label>
                                    <input type="text" name="nama_produk" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Jenis Produk (*)</label>
                                    <input type="text" name="jenis_produk" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Tahun Data (*)</label>
                                    <input type="text" name="tahun_data" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Tenaga Kerja Laki (*)</label>
                                    <input type="text" name="tk_laki" placeholder="Isikan angka saja" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Tenaga Kerja Perempuan (*)</label>
                                    <input type="text" name="tk_perempuan" placeholder="Isikan angka saja" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Nilai Investasi (Rp.000) (*)</label>
                                    <input type="text" name="nilai_investasi" placeholder="Isikan angka saja tanpa `Rp`" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Jumlah Kapasitas Produksi (*)</label>
                                    <input type="text" name="kapasitas_produksi" placeholder="Isikan angka saja" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Satuan (*)</label>
                                    <input type="text" name="satuan" placeholder="" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Nilai Produksi (Rp.000 (*))</label>
                                    <input type="text" name="nilai_produksi" placeholder="Isikan angka saja tanpa `Rp`" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Nilai BB (Rp.000) (*)</label>
                                    <input type="text" name="nilai_bb" placeholder="Isikan angka saja tanpa `Rp`" value="" class="form-control" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Pemasaran Ekspor (%)</label>
                                    <input type="text" name="pemasaran_ekspor" placeholder="Isikan angka saja tanpa `%`" value="" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mb-30">
                                    <label>Negara Tujuan Ekspor</label>
                                    <input type="text" name="negara_ekspor" placeholder="" value="" class="form-control">
                                </div>
                            </div>

                           


                            <div class="row">
                                <div class="col-md-12 ">
                                    <input type="submit" value="DAFTAR SEKARANG" class="button medium rounded gray font-open-sans" data-loading-text="Loading...">
                                </div>
                            </div>

                        </form>   
                        <div class="alert alert-success hidden" id="contactSuccess">
                            <strong>Success!</strong> Your message has been sent to us.
                        </div>

                        <div class="alert alert-danger hidden" id="contactError">
                            <strong>Error!</strong> There was an error sending your message.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-form-cont">
                <div class="mb-40">
                    <h3 >Seputar IKM</h3>
                </div>
                <dl class="accordion accordion-bg-gray">
                    <?php foreach(page('faq')->children()->find($page->uid())->children() as $faq): ?>
                    <dt>
                        <a href="#"><?php echo $faq->title() ?></a>
                    </dt>
                    <dd>
                        <?php echo $faq->text() ?>
                    </dd>
                    <?php endforeach; ?>
                </dl>
                </div>
            </div>

        </div>
    </div>
</div>