<!-- FAVICONS -->
<link rel="shortcut icon" href="<?php echo kirby()->urls()->assets(),('/images/favicon/favicon.png'); ?>">
<link rel="apple-touch-icon" href="<?php echo kirby()->urls()->assets(),('/images/favicon/apple-touch-icon.png'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo kirby()->urls()->assets(),('/images/favicon/apple-touch-icon-72x72.png'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo kirby()->urls()->assets(),('/images/favicon/apple-touch-icon-114x114.png'); ?>">
<link rel="icon" sizes="192x192" href="<?php echo kirby()->urls()->assets(),('/images/favicon/icon-192x192.png'); ?>">
