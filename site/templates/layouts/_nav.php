<div class="main-menu-container">
    <div class="container-m-30 clearfix"> 
                          
        <!-- MAIN MENU -->
        <div id="main-menu">
            <div class="navbar navbar-default" role="navigation">

                <!-- MAIN MENU LIST -->
                <nav class="collapse collapsing navbar-collapse right-1024">
                    <ul class="nav navbar-nav">
                        <?php $current_page_id = $page->id() ?>
                        <?php foreach($pages->visible() as $navitem): ?>
                            <?php if($current_page_id == $navitem->id()) : ?>
                                <?php $is_active = true; ?>
                            <?php else: ?>
                                <?php $is_active = false; ?>
                            <?php endif; ?>
                        <li class="parent <?php if($is_active == true) echo "current" ?>">
                            <a href="<?php echo $navitem->url() ?>" class="<?php if($is_active == true) echo "current" ?>">
                                <div class="main-menu-title"><?php echo $navitem->title()->html() ?></div>
                            </a>
                            
                            <?php if($navitem->hasChildren()) : ?>
                            <ul class="sub">
                                <?php foreach($navitem->children() as $subnavitem) : ?>
                                <li><a class="" href="<?php echo $subnavitem->url() ?>"><?php echo $subnavitem->title() ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>

                        </li>
                        <?php endforeach; ?>
                        </ul>
                </nav>
   
            </div>
        </div>
                                
    </div>
</div>
